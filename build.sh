#!/bin/sh

echo "Running command-tree-parser build script"
echo "command-tree-parser: typescript build"
tsc --declaration
if command -v node >/dev/null 2>&1; then
  echo "command-tree-parser: node test runner"
  node --test dist/test.js
else
  echo "command-tree-parser: skipping test runner, Node.js is not installed"
fi
