import test from "node:test";
import assert from "node:assert";

import CTP, {
  isNoMatch,
  isOK,
  noMatch,
  ok,
  ParseResult,
  resp1,
  resp2,
  resp3,
  Transformers as T,
} from "./index";

test("single layer", async (t) => {
  await t.test("simple match", () => {
    const ctp = new CTP([["hello", "hello to you"]]);

    const response = ctp.parse("hello");
    assert.equal(response.value, "hello to you");
  });
  await t.test("simple no-match", () => {
    const ctp = new CTP([["hello", "hello to you"]]);

    const response = ctp.parse("hola");
    assert(isNoMatch(response));
  });
  await t.test("case-sensitive match", () => {
    const ctp = new CTP([["Hello", "hello to you"]], {
      caseInsensitive: false,
    });

    const response = ctp.parse("Hello");
    assert.equal(response.value, "hello to you");
  });

  await t.test("case-sensitive no-match", () => {
    const ctp = new CTP([["hello", "hello to you"]], {
      caseInsensitive: false,
    });

    const response = ctp.parse("HELLO");
    assert(isNoMatch(response));
  });

  await t.test("case-INsensitive match", () => {
    const ctp = new CTP([["hello", "hello to you"]], {
      caseInsensitive: true,
    });

    const response = ctp.parse("HELLO");
    assert.equal(response.value, "hello to you");
  });

  await t.test("match first item first", () => {
    const ctp = new CTP([
      ["hello", "response 1"],
      ["hello", "response 2"],
    ]);

    const response = ctp.parse("hello");
    assert.equal(response.value, "response 1");
  });

  await t.test("match second item only", () => {
    const ctp = new CTP([
      ["hello", "response 1"],
      ["hello!", "response 2"],
    ]);

    const response = ctp.parse("hello!");
    assert.equal(response.value, "response 2");
  });
});

test("complex", async (t) => {
  await t.test("two word match", () => {
    const ctp = new CTP([["hello", "there", "hello to you"]]);

    const response1 = ctp.parse("hello");
    assert(isNoMatch(response1));

    const response2 = ctp.parse("hello there");
    assert.equal(response2.value, "hello to you");
  });

  await t.test("match input without value result", () => {
    const ctp = new CTP([["a", "b"]]);

    const response1 = ctp.parse("a b");
    assert(isOK(response1));
  });

  await t.test(
    "do not match input without value result with too many params",
    () => {
      const ctp = new CTP([["a", "b"]]);

      const response1 = ctp.parse("a b c");
      assert(isNoMatch(response1));
    },
  );

  await t.test(
    "extra param is not accepted to a terminal resp function",
    () => {
      const ctp = new CTP([["a", "b", resp1(() => ok(null))]]);

      const response1 = ctp.parse("a b c");
      assert(isNoMatch(response1));
    },
  );

  await t.test(
    "extra param IS accepted to a non-terminal resp function and it is optional",
    () => {
      const ctp = new CTP([["a", "b", (_lastInput) => ok(null)]]);

      const response1 = ctp.parse("a b c");
      assert(isOK(response1));
      const response2 = ctp.parse("a b");
      assert(isOK(response2));
    },
  );

  await t.test("branched match", () => {
    const ctp = new CTP([
      [
        "hello",
        [
          ["there", "hello to you"],
          ["friend", "hello my friend"],
        ],
      ],
    ]);

    const response1 = ctp.parse("hello there");
    assert.equal(response1.value, "hello to you");

    const response2 = ctp.parse("hello friend");
    assert.equal(response2.value, "hello my friend");
  });

  const add = resp2((a: number, b: number) => ok(a + b));

  const ctp = new CTP([["add", T.NUMBER, T.NUMBER, add]]);

  await t.test("function matchers", () => {
    const response1 = ctp.parse("add a b");
    assert(isNoMatch(response1));

    const response2 = ctp.parse("add 3 7");
    assert.equal(response2.value, 10);
  });

  await t.test("multi input (word) function matcher #1", () => {
    const echoParser = (
      _current: string,
      _previous: any[],
      remaining: string[],
    ): ParseResult<string> => {
      return ok(remaining.join(" "), remaining.length);
    };

    const ctp2 = new CTP([[echoParser]]);

    const response = ctp2.parse("please reply with this!");

    assert.equal(response.value, "please reply with this!");
  });

  await t.test("multi input (word) function matcher #2", () => {
    const thanksParser = (
      _current: string,
      _previous: any[],
      remaining: string[],
    ): ParseResult<string> => {
      if (
        /thanks,?/.test(remaining.join(" ")) ||
        /thank you,?/.test(remaining.join(" "))
      ) {
        return ok(remaining[remaining.length - 1], remaining.length);
      } else {
        return noMatch;
      }
    };

    const ctp2 = new CTP([[thanksParser]]);

    const response1 = ctp2.parse("thanks, john");
    assert.equal(response1.value, "john");
    const response2 = ctp2.parse("thank you, john");
    assert.equal(response2.value, "john");
    const response3 = ctp2.parse("bye");
    assert.equal(isOK(response3), false);
  });

  await t.test("multi input matcher consumes variable number of items", () => {
    const takeNumbers = (
      _current: string,
      _previous: any[],
      _remaining: string[],
    ): ParseResult<number[]> => {
      let result: number[] = [];
      for (const x of _remaining) {
        if (Number.isFinite(parseFloat(x))) {
          result.push(parseFloat(x));
        } else {
          break;
        }
      }

      if (result.length === 0) {
        return noMatch;
      } else {
        return ok(result, result.length);
      }
    };

    const ctp2 = new CTP([
      [
        "take",
        [
          [
            takeNumbers,
            "product",
            resp2((numbers: number[]) => ok(numbers[0] * numbers[1])),
          ],
          [
            takeNumbers,
            "sum",
            resp2((numbers: number[]) =>
              ok(numbers[0] + numbers[1] + numbers[2]),
            ),
          ],
        ],
      ],
    ]);

    const response1 = ctp2.parse("take 2 3 product");
    assert.equal(response1.value, 6);
    const response2 = ctp2.parse("take 2 3 4 sum");
    assert.equal(response2.value, 9);
    const response3 = ctp2.parse("take five product");
    assert(isNoMatch(response3));
  });

  await t.test("optional input at the end", () => {
    const optionalNumber = (current: string) => {
      return ok(current ? parseFloat(current) : null, current ? 1 : 0);
    };

    const ctp2 = new CTP([
      [
        "sumopt",
        T.NUMBER,
        optionalNumber,
        resp2((a: number, b: number | null) => ok(a + (b ?? 0))),
      ],
    ]);

    const response1 = ctp2.parse("sumopt 1 2");
    assert.equal(response1.value, 3);
    const response2 = ctp2.parse("sumopt 1");
    assert.equal(response2.value, 1);
    const response3 = ctp2.parse("sumopt 1 2 3");
    assert.equal(isOK(response3), false);
  });

  await t.test("optional input at front", () => {
    const optionalNumber = (current: string) => {
      return Number.isFinite(parseFloat(current))
        ? ok(parseFloat(current), 1)
        : ok(null, 0);
    };

    const ctp2 = new CTP([
      [
        optionalNumber,
        "plus",
        T.NUMBER,
        resp3((a: number | null, _: string, b: number) => ok((a ?? 0) + b)),
      ],
    ]);

    const response1 = ctp2.parse("plus 2");
    assert.equal(response1.value, 2);
    const response2 = ctp2.parse("3 plus 2");
    assert.equal(response2.value, 5);
    const response3 = ctp2.parse("x plus 3");
    assert.equal(isOK(response3), false);
  });
});
